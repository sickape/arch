#!/bin/bash

echo "-------------------------------------------------------"
echo "START INSTALLING base, linux, linux-firmware to /mnt"
echo "-------------------------------------------------------"

pacstrap /mnt base linux linux-firmware
genfstab -U /mnt >> /mnt/etc/fstab

mkdir test/
cp -r newroot/* test/

echo "-------------------------------------------------------"
echo "SUCCESS INSTALL NOW YOU CAN USE arch-chroot /mnt"
echo "-------------------------------------------------------"