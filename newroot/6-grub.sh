#!bin/bash

pacman -S grub efibootmgr
grub-install --target=x86_64-efi --efi-directory=/efi/ --bootloader-id=Arch
grub-mkconfig -o /boot/grub/grub.cfg

echo "-------------------------------------------------------"
echo "SUCCESS GRUB"
echo "-------------------------------------------------------"