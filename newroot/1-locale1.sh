#!/bin/bash

pacman -S nano
ln -sf /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
hwclock --systohc

echo "-------------------------------------------------------"
echo "EDIT /etc/locale.gen"
echo "-------------------------------------------------------"